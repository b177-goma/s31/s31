// Set up the dependecies

const express = require ("express");
const mongoose = require ("mongoose");
const taskRoute = require("./routes/taskRoutes");


// Server setup

const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
mongoose.connect("mongodb+srv://alvin-estiva:gghu0C6AA0JwLlYA@mongodb-cluster.0zh91.mongodb.net/b177-to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

app.use("/tasks", taskRoute)
// Server Listening 
app.listen(port, () => console.log(`Now listening to port ${port}`));
