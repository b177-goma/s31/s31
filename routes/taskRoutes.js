const express = require ("express");

// Create a Router instance that function as a routing system.
const router = express.Router()

// Import the taskControllers
const taskController  = require("../controllers/taskController");

// Route to get all the tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(
		resultFromController));
})

// Route to create a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(
		resultFromController));
})

// Route to delete a task
// endpoint: localhost:3001/tasks/123456
router.delete("/:id", (req, res) =>{
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route to update a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body)
})


// Process a GET request at the "/tasks/:id" route using postman to get a specific task.

router.put("/tasks/:id", (req, res) => {
	taskController.updateTask(req.params.status, req.body)
})

// Create a route for changing the status of a task to "complete".

router.put("/tasks/:id", (req, res) => {
	taskController.updateTask(req.params.status, req.body)
})

// Create a controller function for changing the status of a task to "complete".

router.put("/tasks/:id/complete", (req, res) => {
	taskController.updateTask(req.params.status, req.body)
})


// Export the router to be used in index.js
module.exports = router;