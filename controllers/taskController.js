const Task = require("../models/task");

// Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
} 

// Controller function for creating a task
module.exports.createTask = (requestBody) => {
	// Create a task object based onthe mongoose model "Task"
	let newTask = new Task ({
		// Sets the "name" property with the value received from the client
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}
		// Save is successful 
		else{
			return task;
		}
	})
}

// Create a controller function for deleting a task. 
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}
	})
}

// Controller function for updating a task
module.exports.updateTask = (taskId, newContent) =>{
	return Task.findById(taskId).then((result, error) => {
		if (error){
			console.log(err);
			return false
		}

		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updatedTask
			}
		})
	})
}